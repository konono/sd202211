### 誌面のURL間違い
Software Design 2022年11月号の新生「Ansible」徹底解説の中に出てくる図4のgit cloneコマンドのURLが間違っていたため、こちらに変更をお願いします。

git clone https://github.com/konono/sd202211
